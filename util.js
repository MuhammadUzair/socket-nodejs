module.exports = function() {
  this.sum = function(a, b) {
    return a + b;
  };
  this.multiply = function(a, b) {
    return a * b;
  };
};

export function validNick(nickname) {
  var regex = /^\w*$/;
  return regex.exec(nickname) !== null;
}

export function findIndex(arr, id) {
  var len = arr.length;

  while (len--) {
    if (arr[len].id === id) {
      return len;
    }
  }

  return -1;
}

export function sanitizeString(message) {
  return message.replace(/(<([^>]+)>)/gi, '').substring(0, 35);
}
